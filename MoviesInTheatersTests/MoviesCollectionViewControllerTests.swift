//
//  MoviesCollectionViewControllerTests.swift
//  MoviesInTheatersTests
//
//  Created by Florentin on 30/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import XCTest
@testable import MoviesInTheaters

class MoviesCollectionViewControllerTests: XCTestCase {

    var moviesCollectionViewController: MoviesCollectionViewController!
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        
        moviesCollectionViewController = nil
        super.tearDown()
    }
    
 
//    func testInitialisation(){
//
//        XCTAssertNotNil(moviesCollectionViewController, "ViewController could not be loaded")
//        //XCTAssertNotNil(moviesCollectionViewController.view, "View could not be loaded")
//        //XCTAssertNotNil(moviesCollectionViewController.collectionView, "CollectionView could not be loaded")
//    }
    
//    func testFunctionsAvailable(){
//        
//        XCTAssertTrue(moviesCollectionViewController.responds(to: #selector(MoviesCollectionViewController.showMessage)), "showMessage method not found")
//    }
    
}
