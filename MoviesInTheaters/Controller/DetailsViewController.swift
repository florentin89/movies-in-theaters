//
//  DetailsViewController.swift
//  MoviesInTheaters
//
//  Created by Florentin on 27/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit

class DetailsViewController: UITableViewController {
    
    // MARK: - ***** Properties *****
    var selectedMovie: Movie?
    
    
    // MARK: - ***** Life Cycle Methods *****
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedMovie?.title
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 700
        tableView.rowHeight = 700
    }
    
    //MARK: - ***** TableView Datasource Methods *****
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellDetails", for: indexPath) as? CustomDetailsTableViewCell
        
        cell?.configure(movie: selectedMovie!)
        
        return cell!
    }
    
    //MARK: - ***** TableView Delegate Methods *****
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
