//
//  MoviesCollectionViewController.swift
//  MoviesInTheaters
//
//  Created by Florentin on 27/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit

class MoviesCollectionViewController: UICollectionViewController {
    
    // MARK: - ***** Properties *****
    var moviesArray = [Movie]()
    var page = 1
    var totalMoviesAvailable = 0
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.register(UINib.init(nibName: "DownloadMoreMoviesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DownloadMoreMoviesCollectionViewCell")
        getMovies()
    }
    
    // MARK: ***** UICollectionView DataSource Methods *****
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if((moviesArray.count > 0) && moreMoviesAvailable() && !refreshControl.isRefreshing){
            
            return moviesArray.count + 1
        } else{
            return moviesArray.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row == moviesArray.count) && moreMoviesAvailable(){
            
            var getMoreMovies = collectionView.dequeueReusableCell(withReuseIdentifier: "DownloadMoreMoviesCollectionViewCell", for: indexPath) as? DownloadMoreMoviesCollectionViewCell
            if getMoreMovies == nil{
                getMoreMovies = DownloadMoreMoviesCollectionViewCell(frame: CGRect(x: 0, y: 0, width: 175, height: 175))
            }
            getMoreMovies?.loadingSpinner.startAnimating()
            
            return getMoreMovies!
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellMovie", for: indexPath) as? CustomCollectionViewCell
        
        let movie = moviesArray[indexPath.row]
        DispatchQueue.main.async {
            cell?.configure(movie: movie)
        }
        return cell!
    }
    
    //MARK: - ***** CollectionView Delegate Methods *****
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "goToDetails", sender: self)
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DetailsViewController
        
        if let indexPath = collectionView?.indexPathsForSelectedItems?.first {
            destinationVC.selectedMovie = moviesArray[indexPath.row]
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let temporaryCell = cell as? DownloadMoreMoviesCollectionViewCell
        
        if(refreshControl.isRefreshing && (indexPath.row == moviesArray.count)){
            temporaryCell?.loadingSpinner.isHidden = true
            return
        }
        else if(indexPath.row == moviesArray.count){
            temporaryCell?.loadingSpinner.isHidden = false
        }
        
        if((indexPath.row == moviesArray.count) && moreMoviesAvailable())
        {
            page += 1
            getMovies()
        }
    }
    
    // Check if are available more movies to load on the page
    func moreMoviesAvailable() -> Bool {
        if(moviesArray.count < totalMoviesAvailable){
            return true
        } else{
            return false
        }
    }
    
    // Get Movies from themoviedb API server
    func getMovies(){
        
        let url = Constants.baseURL + Constants.pageNo + String(page) + Constants.apiKey
        
        if(page == 1){
            Spinner.show("Loading Movies")
        }
        
        WebServices.getRequest(urlString: url, successBlock: { [weak self] (response) in
            
            Spinner.hide()
            
            guard let movies = response["results"] as? NSArray else{
                return
            }
            
            self?.totalMoviesAvailable = response["total_results"] as! Int
            
            for movie in movies
            {
                self?.moviesArray.append(Movie(dictionary: movie as! NSDictionary))
            }
            
            self?.collectionView?.reloadData()
            
        }) {[weak self] (errorMessage) in
            Spinner.hide()
            self?.showMessage(message: errorMessage)
        }
    }
    
    // Function to generate a custom alert message
    @objc func showMessage(message: String)
    {
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert);
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
