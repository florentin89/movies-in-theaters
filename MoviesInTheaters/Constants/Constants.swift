//
//  Constants.swift
//  MoviesInTheaters
//
//  Created by Florentin on 28/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import Foundation
import UIKit

enum Constants {
    
    static let baseURL = "https://api.themoviedb.org/3/movie/now_playing?"
    static let apiKey = "&api_key=6939a8bba1705ae0db23e913cb1e850b"
    static let pageNo = "page="

}
