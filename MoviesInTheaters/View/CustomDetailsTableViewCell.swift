//
//  CustomTableViewCell.swift
//  MoviesInTheaters
//
//  Created by Florentin on 28/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit

class CustomDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var posterMovieImageView: UIImageView!
    @IBOutlet weak var titleMovieLabel: UILabel!
    @IBOutlet weak var overviewMovieLabel: UILabel!
    @IBOutlet weak var releaseDateMovieLabel: UILabel!
    @IBOutlet weak var userScoreMovieLabel: UILabel!
    
    var movie: Movie!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(movie: Movie){
        
        posterMovieImageView.imageFromServerURL(urlString: movie.posterUrl)
        posterMovieImageView.contentMode = .scaleToFill
        titleMovieLabel.text = movie.title
        overviewMovieLabel.text = "Overview: " + movie.overview
        releaseDateMovieLabel.text = "Release Date: " + movie.releaseDate
        userScoreMovieLabel.text = "User score: " + String(movie.popularity)
    }
}
