//
//  DownloadMoreMoviesCollectionViewCell.swift
//  MoviesInTheaters
//
//  Created by Florentin on 28/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit

class DownloadMoreMoviesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        loadingSpinner.startAnimating()
    }

}
