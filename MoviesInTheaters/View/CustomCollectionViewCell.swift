//
//  CustomCollectionViewCell.swift
//  MoviesInTheaters
//
//  Created by Florentin on 27/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    
    func configure(movie: Movie){
        
        movieTitleLabel.text = movie.title
        moviePosterImageView.imageFromServerURL(urlString: movie.posterUrl)
        moviePosterImageView.clipsToBounds = true
        moviePosterImageView.layer.cornerRadius = moviePosterImageView.frame.height / 2
        moviePosterImageView.contentMode = .scaleToFill
    }
}
