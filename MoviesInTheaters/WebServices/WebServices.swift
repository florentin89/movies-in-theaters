//
//  WebServices.swift
//  MoviesInTheaters
//
//  Created by Florentin on 28/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import Foundation

class WebServices
{
    class func getRequest( urlString: String, successBlock :@escaping (_ response :AnyObject)->Void, errorMsg:@escaping (_ errorMessage :String)->Void )
    {
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            DispatchQueue.main.async {
                if(error == nil)
                {
                    do {
                        let responseJSON = try JSONSerialization.jsonObject(with: data!) as? [String: Any]
                        guard let _ = responseJSON else {
                            errorMsg("Some error has been occurred!")
                            return
                        }
                        successBlock(responseJSON as AnyObject)
                    }
                    catch
                    {
                        errorMsg("Some error has been occurred!")
                    }
                }
                else
                {
                    errorMsg(error!.localizedDescription)
                }
            }
        }
        task.resume()
    }
}
