//
//  Extensions.swift
//  MoviesInTheaters
//
//  Created by Florentin on 28/07/2018.
//  Copyright © 2018 Florentin Lupascu. All rights reserved.
//

import Foundation
import UIKit

public extension UIImageView {
    func imageFromServerURL(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })
        }).resume()
    }
}
